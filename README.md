# Aargh, ma fille a ruiné les cartes de welcome to !

Un petit monstre de deux ans s'est amusé à jeter les cartes de tous mes jeux de société. Hélas plusieurs d'entre elles sont tombées dans la poubelles de tri et ont été embarquées par les éboueurs.

Vous êtes mon ultime recours face à ce drame.
Pourriez-vous sauver mon jeu 'Welcome to' ?

Vous ne connaissez pas le jeu ? Pas bien grave...
Votre objectif est de gérer deux piles de cartes :

- Une pile de cartes 'numéro de maison' allant de 3 à 15.
- Une pile de cartes 'action'

N'étant pas très doué en informatique, il me faudrait également une interface web réalisée en html, css et javascript. Permettant de faire défiler les 27 tours du jeu.

L'inventaire des cartes est listé au sein de la page 2 du **doc/regles.pdf**

## L'interface attendue

L'interface doit présenter les cartes en trois tas distincts. Le tas des cartes 'numéro de maison' divisé en trois tas, le tas des cartes 'action' divisées en trois tas.

On doit donc voir sur la page :

- numero maison tas 1 - action tas 1
- numero maison tas 2 - action tas 2
- numero maison tas 3 - action tas 3

En cliquant sur un bouton **Prochain Tour**, on avance d'une carte chaque pile.

A vous de planifier en équipes les taches à réaliser.

L'objectif étant donc de planifier par le biais des issues l'ensembles des taches à réaliser:

- Chaque tache étant listée en tant qu'Issue
- Chaque tache étant assignée à un étudiant, puis évaluée en charge via un poker planning.
- Respectez les branches attendues au sein de git flow
